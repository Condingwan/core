/*
 Navicat Premium Data Transfer

 Source Server         : test
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 06/01/2021 17:22:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_cart
-- ----------------------------
DROP TABLE IF EXISTS `t_cart`;
CREATE TABLE `t_cart`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NULL DEFAULT NULL,
  `goods_id` int NULL DEFAULT NULL,
  `price` decimal(10, 2) NULL DEFAULT NULL,
  `quantity` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `selected` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `item_id` int NULL DEFAULT NULL COMMENT '主产品编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_cart
-- ----------------------------
INSERT INTO `t_cart` VALUES (5, 4, 208, NULL, '2', '0', '2021-01-05 15:10:07', '2021-01-05 15:17:45', NULL);
INSERT INTO `t_cart` VALUES (9, 1, 207, NULL, '1', '1', '2021-01-06 17:22:03', '2021-01-06 17:22:03', NULL);

-- ----------------------------
-- Table structure for t_category
-- ----------------------------
DROP TABLE IF EXISTS `t_category`;
CREATE TABLE `t_category`  (
  `id` int NOT NULL,
  `parent_id` int NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_category
-- ----------------------------

-- ----------------------------
-- Table structure for t_goods
-- ----------------------------
DROP TABLE IF EXISTS `t_goods`;
CREATE TABLE `t_goods`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category_id` int NULL DEFAULT NULL,
  `subtitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `main_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `detail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `price` decimal(10, 2) NULL DEFAULT NULL,
  `stock` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 209 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_goods
-- ----------------------------
INSERT INTO `t_goods` VALUES (207, '苹果笔记本', 100074, '电脑', '4.jpg', 'mac笔记本不错', 123.00, '7997', '2021-01-05 15:45:31', '2021-01-05 17:02:33');
INSERT INTO `t_goods` VALUES (208, '松下电视', 100074, '小电视', '14.jpg', '这个电视不错', 236.00, '8000', '2020-12-28 15:41:55', '2021-01-05 17:03:26');

-- ----------------------------
-- Table structure for t_level
-- ----------------------------
DROP TABLE IF EXISTS `t_level`;
CREATE TABLE `t_level`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NULL DEFAULT NULL,
  `level` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `exp` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_level
-- ----------------------------
INSERT INTO `t_level` VALUES (1, NULL, '1', '0', '2020-12-22 13:59:25', '2020-12-22 13:59:25');
INSERT INTO `t_level` VALUES (2, NULL, '1', '0', '2021-01-05 15:09:48', '2021-01-05 15:09:48');
INSERT INTO `t_level` VALUES (3, NULL, '1', '0', '2021-01-05 17:06:43', '2021-01-05 17:06:43');
INSERT INTO `t_level` VALUES (4, NULL, '1', '0', '2021-01-05 17:11:20', '2021-01-05 17:11:20');
INSERT INTO `t_level` VALUES (5, NULL, '1', '0', '2021-01-06 17:13:29', '2021-01-06 17:13:29');
INSERT INTO `t_level` VALUES (6, NULL, '1', '0', '2021-01-06 17:17:50', '2021-01-06 17:17:50');
INSERT INTO `t_level` VALUES (7, NULL, '1', '0', '2021-01-06 17:21:36', '2021-01-06 17:21:36');
INSERT INTO `t_level` VALUES (8, NULL, '1', '0', '2021-01-06 17:22:12', '2021-01-06 17:22:12');

-- ----------------------------
-- Table structure for t_order
-- ----------------------------
DROP TABLE IF EXISTS `t_order`;
CREATE TABLE `t_order`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NULL DEFAULT NULL,
  `shipping_id` int NULL DEFAULT NULL,
  `order_no` int NULL DEFAULT NULL,
  `payment` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `payment_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `postage` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `payment_time` datetime(0) NULL DEFAULT NULL,
  `delivery_time` datetime(0) NULL DEFAULT NULL,
  `receive_time` datetime(0) NULL DEFAULT NULL,
  `return_goods_time` datetime(0) NULL DEFAULT NULL,
  `refund_time` datetime(0) NULL DEFAULT NULL,
  `close_time` datetime(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_order
-- ----------------------------
INSERT INTO `t_order` VALUES (1, 1, 1, 1609, '944.0', '0', '0', '2', NULL, NULL, NULL, NULL, NULL, '2020-12-28 17:37:19', '2020-12-28 17:09:46', '2021-01-06 16:57:11');
INSERT INTO `t_order` VALUES (2, 1, 1, 1609, '236.0', '0', '0', '2', NULL, NULL, NULL, NULL, NULL, '2021-01-05 15:41:46', '2020-12-30 16:59:11', '2021-01-06 16:57:22');
INSERT INTO `t_order` VALUES (3, 1, 1, 1609, '236.0', '0', '0', '2', NULL, NULL, NULL, NULL, NULL, '2021-01-05 15:41:45', '2020-12-30 17:03:01', '2021-01-06 16:58:20');
INSERT INTO `t_order` VALUES (4, 1, 1, 110931425, '236.0', '0', '0', '2', NULL, NULL, NULL, NULL, NULL, '2021-01-05 17:03:27', '2021-01-05 11:09:31', '2021-01-06 17:01:21');
INSERT INTO `t_order` VALUES (5, 1, 1, 154705138, '482.0', '0', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-05 15:47:05', '2021-01-05 15:47:05');
INSERT INTO `t_order` VALUES (6, 1, 1, 170233882, '123.0', '0', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-05 17:02:33', '2021-01-05 17:02:33');

-- ----------------------------
-- Table structure for t_order_item
-- ----------------------------
DROP TABLE IF EXISTS `t_order_item`;
CREATE TABLE `t_order_item`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NULL DEFAULT NULL,
  `order_id` int NULL DEFAULT NULL,
  `goods_id` int NULL DEFAULT NULL,
  `order_no` int NULL DEFAULT NULL,
  `quantity` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `main_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `detail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `price` decimal(10, 2) NULL DEFAULT NULL,
  `total_price` decimal(10, 2) NULL DEFAULT NULL,
  `refund_time` datetime(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_order_item
-- ----------------------------
INSERT INTO `t_order_item` VALUES (1, 1, 1, 208, 1609, '4', '松下电视', NULL, '0', '这个电视不错', 236.00, 944.00, NULL, '2020-12-28 17:09:46', '2020-12-28 17:09:46');
INSERT INTO `t_order_item` VALUES (2, 1, 2, 208, 1609, '1', '松下电视', NULL, '0', '这个电视不错', 236.00, 236.00, NULL, '2020-12-30 16:59:11', '2020-12-30 16:59:11');
INSERT INTO `t_order_item` VALUES (3, 1, 3, 208, 1609, '1', '松下电视', NULL, '0', '这个电视不错', 236.00, 236.00, NULL, '2020-12-30 17:03:01', '2020-12-30 17:03:01');
INSERT INTO `t_order_item` VALUES (4, 1, 4, 208, 110931425, '1', '松下电视', NULL, '0', '这个电视不错', 236.00, 236.00, NULL, '2021-01-05 11:09:31', '2021-01-05 11:09:31');
INSERT INTO `t_order_item` VALUES (5, 1, 5, 208, 154705138, '1', '松下电视', '14.jpg', '0', '这个电视不错', 236.00, 236.00, NULL, '2021-01-05 15:47:05', '2021-01-05 15:47:05');
INSERT INTO `t_order_item` VALUES (6, 1, 5, 207, 154705138, '2', '苹果笔记本', '4.jpg', '0', 'mac笔记本不错', 123.00, 246.00, NULL, '2021-01-05 15:47:05', '2021-01-05 15:47:05');
INSERT INTO `t_order_item` VALUES (7, 1, 6, 207, 170233882, '1', '苹果笔记本', '4.jpg', '0', 'mac笔记本不错', 123.00, 123.00, NULL, '2021-01-05 17:02:33', '2021-01-05 17:02:33');

-- ----------------------------
-- Table structure for t_payinfo
-- ----------------------------
DROP TABLE IF EXISTS `t_payinfo`;
CREATE TABLE `t_payinfo`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NULL DEFAULT NULL,
  `order_no` int NULL DEFAULT NULL,
  `pay_platform` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `platform_number` int NULL DEFAULT NULL,
  `platform_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_payinfo
-- ----------------------------

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NULL DEFAULT NULL,
  `role_no` int NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES (1, NULL, 1, '2020-12-22 13:59:25', '2020-12-22 13:59:25');
INSERT INTO `t_role` VALUES (2, NULL, 1, '2021-01-05 15:09:48', '2021-01-05 15:09:48');
INSERT INTO `t_role` VALUES (3, NULL, 1, '2021-01-05 17:06:43', '2021-01-05 17:06:43');
INSERT INTO `t_role` VALUES (4, NULL, 1, '2021-01-05 17:11:20', '2021-01-05 17:11:20');
INSERT INTO `t_role` VALUES (5, NULL, 1, '2021-01-06 17:13:29', '2021-01-06 17:13:29');
INSERT INTO `t_role` VALUES (6, NULL, 1, '2021-01-06 17:17:50', '2021-01-06 17:17:50');
INSERT INTO `t_role` VALUES (7, NULL, 1, '2021-01-06 17:21:36', '2021-01-06 17:21:36');
INSERT INTO `t_role` VALUES (8, NULL, 1, '2021-01-06 17:22:12', '2021-01-06 17:22:12');

-- ----------------------------
-- Table structure for t_shipping
-- ----------------------------
DROP TABLE IF EXISTS `t_shipping`;
CREATE TABLE `t_shipping`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `province` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `district` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `street` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_shipping
-- ----------------------------
INSERT INTO `t_shipping` VALUES (1, 1, '1212', '3121', '3123', '1221', '2113', '21', '1312', '21', '2020-12-28 17:02:39', '2020-12-28 17:02:39');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键Id',
  `username` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话',
  `email` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `question` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问题',
  `answer` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '回答',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `item_id` int NULL DEFAULT NULL COMMENT '主产品编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES (1, 'wan', 'wan', '123231', '123123', 'undefined', 'undefined', '2020-12-07 11:02:31', '2021-01-06 16:20:06', NULL);
INSERT INTO `t_user` VALUES (3, 'wjm', 'YES', '1111', '1111', NULL, NULL, '2020-12-22 13:59:23', '2021-01-06 12:48:10', NULL);
INSERT INTO `t_user` VALUES (4, 'wan1', 'wan', '3243', '423423', '4234', '234', '2021-01-05 15:09:48', '2021-01-05 15:20:47', NULL);
INSERT INTO `t_user` VALUES (6, 'wan111', 'wan', NULL, NULL, NULL, NULL, '2021-01-05 17:11:20', '2021-01-05 17:11:20', NULL);
INSERT INTO `t_user` VALUES (7, 'wan1111', 'wan', NULL, NULL, NULL, NULL, '2021-01-06 17:13:29', '2021-01-06 17:13:29', NULL);
INSERT INTO `t_user` VALUES (8, 'wwawa', 'wawawa', NULL, NULL, NULL, NULL, '2021-01-06 17:17:50', '2021-01-06 17:17:50', NULL);
INSERT INTO `t_user` VALUES (9, 'wanwerdawerfa', 'wan', NULL, NULL, NULL, NULL, '2021-01-06 17:21:36', '2021-01-06 17:21:36', NULL);
INSERT INTO `t_user` VALUES (10, 'wanbdvsb', 'wan', NULL, NULL, NULL, NULL, '2021-01-06 17:22:12', '2021-01-06 17:22:12', NULL);

SET FOREIGN_KEY_CHECKS = 1;
