package com.example.admin.db.vo;

import com.example.admin.db.bo.CartGoodsBo;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class CartGoodsVo {

	private List<CartGoodsBo> cartGoodsBoList;
	
	private BigDecimal totalPrice;

}
