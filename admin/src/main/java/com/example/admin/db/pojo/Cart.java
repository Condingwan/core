package com.example.admin.db.pojo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class Cart {
    private Integer id;

    private Integer userId;

    private Integer goodsId;

    private BigDecimal price;

    private Integer quantity;

    private Integer selected;

    private Date createTime;

    private Date updateTime;

    public Cart(Integer id, Integer userId, Integer goodsId, BigDecimal price, Integer quantity, Integer selected, Date createTime, Date updateTime) {
        this.id = id;
        this.userId = userId;
        this.goodsId = goodsId;
        this.price = price;
        this.quantity = quantity;
        this.selected = selected;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public Cart() {

    }
}