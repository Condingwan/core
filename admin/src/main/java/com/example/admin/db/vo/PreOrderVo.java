package com.example.admin.db.vo;

import com.example.admin.db.bo.order.CartGoodsBo;
import com.example.admin.db.pojo.Shipping;
import lombok.Data;

import java.util.List;

@Data
public class PreOrderVo {

	private List<CartGoodsBo> cartGoodsBoList;
	
	private List<Shipping> shippingList;

}
