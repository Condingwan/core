package com.example.admin.db.vo;

import com.example.admin.db.pojo.Order;
import com.example.admin.db.pojo.OrderItem;
import com.example.admin.db.pojo.Shipping;
import lombok.Data;

import java.util.List;

@Data
public class OrderVo {

	private Order order;
	
	private List<OrderItem> orderItemList;
	
	private Shipping shipping;
	
	private String createTime;
}
