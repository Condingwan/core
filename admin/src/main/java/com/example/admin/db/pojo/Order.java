package com.example.admin.db.pojo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class Order {
    private Integer id;

    private Integer userId;

    private Integer shippingId;

    private Long orderNo;

    private BigDecimal payment;

    private Integer paymentType;

    private BigDecimal postage;

    private Integer status;

    private Date paymentTime;

    private Date deliveryTime;

    private Date receiveTime;

    private Date returnGoodsTime;

    private Date refundTime;

    private Date closeTime;

    private Date createTime;

    private Date updateTime;

    public Order(Integer id, Integer userId, Integer shippingId, Long orderNo, BigDecimal payment, Integer paymentType, BigDecimal postage, Integer status, Date paymentTime, Date deliveryTime, Date receiveTime, Date returnGoodsTime, Date refundTime, Date closeTime, Date createTime, Date updateTime) {
        this.id = id;
        this.userId = userId;
        this.shippingId = shippingId;
        this.orderNo = orderNo;
        this.payment = payment;
        this.paymentType = paymentType;
        this.postage = postage;
        this.status = status;
        this.paymentTime = paymentTime;
        this.deliveryTime = deliveryTime;
        this.receiveTime = receiveTime;
        this.returnGoodsTime = returnGoodsTime;
        this.refundTime = refundTime;
        this.closeTime = closeTime;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public Order() {

    }
}