package com.example.admin.db.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class User {
    private Integer id;

    private String username;

    private String password;

    private String phone;

    private String email;

    private String question;

    private String answer;

    private Date createTime;

    private Date updateTime;

    public User() {
    }

    public User(Integer id, String username, String password, String phone, String email, String question, String answer, Date createTime, Date updateTime) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.phone = phone;
        this.email = email;
        this.question = question;
        this.answer = answer;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }
}