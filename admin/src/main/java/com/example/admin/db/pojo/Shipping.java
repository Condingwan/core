package com.example.admin.db.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class Shipping {
    private Integer id;

    private Integer userId;

    private String name;

    private String phone;

    private String province;

    private String city;

    private String district;

    private String street;

    private String address;

    private String zip;

    private Date createTime;

    private Date updateTime;

    public Shipping(Integer id, Integer userId, String name, String phone, String province, String city, String district, String street, String address, String zip, Date createTime, Date updateTime) {
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.phone = phone;
        this.province = province;
        this.city = city;
        this.district = district;
        this.street = street;
        this.address = address;
        this.zip = zip;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }
}