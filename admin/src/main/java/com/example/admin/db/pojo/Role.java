package com.example.admin.db.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class Role {
    private Integer id;

    private Integer userId;

    private Integer roleNo;

    private Date createTime;

    private Date updateTime;

    public Role(Integer id, Integer userId, Integer roleNo, Date createTime, Date updateTime) {
        this.id = id;
        this.userId = userId;
        this.roleNo = roleNo;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public Role() {

    }
}