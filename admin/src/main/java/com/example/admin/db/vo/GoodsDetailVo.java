package com.example.admin.db.vo;

import com.example.admin.db.pojo.Goods;
import lombok.Data;

@Data
public class GoodsDetailVo {

	// Goods pojo
	private Goods goods;
	// extension
	private String imgHost;

}
