package com.example.admin.db.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class Category {
    private Integer id;

    private Integer parentId;

    private String name;

    private Integer status;

    private Date createTime;

    private Date updateTime;

    public Category(Integer id, Integer parentId, String name, Integer status, Date createTime, Date updateTime) {
        this.id = id;
        this.parentId = parentId;
        this.name = name;
        this.status = status;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public Category() {

    }
}