package com.example.admin.utils;

import java.io.Serializable;

public class Result implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final int ERROR_OK = 1001;
    public static final int ERROR_SYSTEM = 10000;
    private int code;
    private String msg;
    private String message;
    public Object data;

    public Result() {
        this.code = 1001;
    }

    public Result(Object data) {
        this.code = 1001;
        this.data = data;
    }

    public Result(int code) {
        this.code = code;
    }

    public Result(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public Result(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Result code(int code) {
        this.code = code;
        return this;
    }

    public Result data(Object data) {
        this.data = data;
        return this;
    }

    public Result fail(String msg) {
        this.code = 10000;
        this.msg = msg;
        return this;
    }

    public Result fail(int code, String msg) {
        this.code = code;
        this.msg = msg;
        return this;
    }

    public Result fail() {
        this.code = 10000;
        return this;
    }


    public int getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }


    public Object getData() {
        return this.data;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setData(Object data) {
        this.data = data;
    }

    protected boolean canEqual(Object other) {
        return other instanceof Result;
    }
}