package com.example.admin.service.interfaces;

import com.example.admin.common.Message;
import com.example.admin.db.pojo.Shipping;
import com.example.admin.db.pojo.User;

public interface IOrderService {

	Message preorder(User user);
	
	Message generateOrder(Integer shippingId, User user);
	
	Message saveOrEditAddress(Shipping shipping, User user);
	
	Message listOrder(User user, int pageNum, int pageSize);
	
	Message cancelOrder(Integer orderId);
	
	Message getPaymentStatus(Integer orderId, Integer userId);

    Message listAllOrder(User user, int i, int i1);

	Message updateStatus(Integer orderId, Integer status, Integer id);
}
