package com.example.admin.service.interfaces;

import com.example.admin.db.pojo.Shipping;
import com.example.admin.utils.Result;

/**
 * @author wjm
 * @data 2020/12/18 13:49
 * @desc
 */
public interface OrderService {
    Result preorder();

    Result getPaymentStatus(Integer orderId);

    Result cancelOrder(Integer orderId);

    Result listOrder(int i, int i1);

    Result saveOrEditAddress(Shipping shipping);

    Result generateOrder(Integer shippingId);
}
