package com.example.admin.service.interfaces;

import com.example.admin.common.Message;
import com.example.admin.db.pojo.Goods;
import com.github.pagehelper.PageInfo;

public interface IGoodsService {

	Message addGoods(Goods goods);
	
	Message<PageInfo> list(Integer categoryId, int pageNum, int pageSize);
	
	Message getGoodsDetail(int goodsId);
	
	Message<PageInfo> searchGoods(String keyword, int pageNum, int pageSize);
}
