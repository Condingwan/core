package com.example.admin.service.interfaces;

import com.example.admin.db.pojo.User;
import com.example.admin.utils.Result;

public interface UserService {
    public Result loginInvalid(User user);

    public Result insertUser(User user);

}
