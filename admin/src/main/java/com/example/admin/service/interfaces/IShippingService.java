package com.example.admin.service.interfaces;

import com.example.admin.common.Message;
import com.example.admin.db.pojo.Shipping;

public interface IShippingService {

	Message addShipping(Shipping shipping, Integer userId);
	
	Message list(Integer userId);

    Message listAll();
}
