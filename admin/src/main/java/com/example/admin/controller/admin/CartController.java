package com.example.admin.controller.admin;

import com.example.admin.common.Const;
import com.example.admin.common.LoginCheck;
import com.example.admin.common.Message;
import com.example.admin.dao.CartMapper;
import com.example.admin.dao.GoodsMapper;
import com.example.admin.dao.OrderItemMapper;
import com.example.admin.db.pojo.OrderItem;
import com.example.admin.db.pojo.User;
import com.example.admin.service.interfaces.ICartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class CartController {

	@Autowired
	private ICartService iCartService;
	@Autowired
	private LoginCheck loginCheck;
	@Autowired
	private CartMapper cartMapper;
	@Autowired
	private GoodsMapper goodsMapper;
	@Autowired 
	private OrderItemMapper orderItemMapper;
	
	@RequestMapping("/go/cart/test")
	@ResponseBody
	public Message test(){
//		List<OrderItem> orderItemList = new ArrayList<>();
//		OrderItem orderItem1 = new OrderItem();
//		OrderItem orderItem2 = new OrderItem();
//		orderItem1.setGoodsId(201);
//		orderItem1.setQuantity(2);
//		orderItem2.setGoodsId(202);
//		orderItem2.setQuantity(2);
//		orderItemList.add(orderItem1);
//		orderItemList.add(orderItem2);
//		int count = goodsMapper.batchUpdateQuantityForCancelOrder(orderItemList);
//		System.out.println(count);
		String str = "1504409920497";
		
		List<OrderItem> orderItemList = orderItemMapper.selectQuantityByOrderNo(new Long(str));
		return null;
	}
	
	@RequestMapping("/**/cart/addCart")
	@ResponseBody
	public Message addCart(Integer goodsId, Integer goodsQuantity, HttpSession session){
		if(!loginCheck.check(session, Const.NORMAL_USER))
			return Message.errorMsg("未登录或无权限");
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		return iCartService.addCart(user, goodsId, goodsQuantity);
	}
	
	@RequestMapping("/*/cart/listCart")
	@ResponseBody
	public Message listCart(HttpSession session){
		if(!loginCheck.check(session, Const.NORMAL_USER))
			return Message.errorMsg("未登录或无权限");
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		return iCartService.listCart(user);
	}
	
	@RequestMapping("/*/cart/changeQuantity")
	@ResponseBody
	public Message changeQuantity(Integer cartId, int quantity, HttpSession session){
		if(!loginCheck.check(session, Const.NORMAL_USER))
			return Message.errorMsg("未登录或无权限");
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		return iCartService.changeQuantity(cartId, quantity, user);
	}
	
	// 单选或全选
	@RequestMapping("/*/cart/select")
	@ResponseBody
	public Message select(Integer cartId, HttpSession session){
		if(!loginCheck.check(session, Const.NORMAL_USER))
			return Message.errorMsg("未登录或无权限");
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		return iCartService.selectOrUnselect(user, cartId, Const.Cart.SELECTED);
	}
	
	// 单不选或全不选
	@RequestMapping("/*/cart/unselect")
	@ResponseBody
	public Message unselect(Integer cartId, HttpSession session) {
		if (!loginCheck.check(session, Const.NORMAL_USER))
			return Message.errorMsg("未登录或无权限");
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		return iCartService.selectOrUnselect(user, cartId, Const.Cart.UNSELECT);
	}
	
	@RequestMapping("/*/cart/delete")
	@ResponseBody
	public Message delete(Integer cartId, HttpSession session) {
		if (!loginCheck.check(session, Const.NORMAL_USER))
			return Message.errorMsg("未登录或无权限");
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		return iCartService.delete(cartId, user);
	}
}
