package com.example.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class BaseController {
    @RequestMapping("/*/{html}")
    public String go(@PathVariable("html") String html){
        System.out.println(html);
        return html;
    }
    @RequestMapping("/{html}")
    public String goods(@PathVariable("html") String html,@RequestParam("scId")int scId, Model model){
        System.out.println(html);
        model.addAttribute(scId);
        System.out.println(scId);
        return html;
    }
    @RequestMapping("/good/{html}")
    public String good(@PathVariable("html") String html,@RequestParam("goodsId")int goodsId, Model model){
        System.out.println(html);
        model.addAttribute(goodsId);
        System.out.println(goodsId);
        return html;
    }

}
