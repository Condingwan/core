package com.example.admin.controller.admin;

import com.example.admin.common.Const;
import com.example.admin.common.LoginCheck;
import com.example.admin.common.Message;
import com.example.admin.db.pojo.Shipping;
import com.example.admin.db.pojo.User;
import com.example.admin.service.interfaces.IShippingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/go/shipping")
public class ShippingController {

	@Autowired
	private LoginCheck loginCheck;
	@Autowired
	private IShippingService iShippingService;
	
	@RequestMapping(value = "/addShipping", method = RequestMethod.POST)
	@ResponseBody
	public Message addShipping(Shipping shipping, HttpSession session){
		if(!loginCheck.check(session, Const.NORMAL_USER))
			return Message.errorMsg("未登录或无权限");
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		return iShippingService.addShipping(shipping, user.getId());
	}
	
	@RequestMapping(value = "/list")
	@ResponseBody
	public Message list(HttpSession session){
		if(!loginCheck.check(session, Const.NORMAL_USER))
			return Message.errorMsg("未登录或无权限");
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		return iShippingService.listAll();
	}
}
