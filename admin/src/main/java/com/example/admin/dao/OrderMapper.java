package com.example.admin.dao;

import com.example.admin.db.pojo.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Order record);

    int insertSelective(Order record);

    Order selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Order record);

    int updateByPrimaryKey(Order record);
    
    List<Order> selectByUserId(Integer userId);
    
    Order selectByOrderNo(Long orderNo);
    
    Order selectByOrderIdUserId(@Param("orderId") Integer orderId, @Param("userId") Integer userId);

    List<Order> listAllOrder();

    void updateStatus(Integer id, Integer status, Integer userId);

}