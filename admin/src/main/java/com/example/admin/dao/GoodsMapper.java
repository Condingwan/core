package com.example.admin.dao;

import com.example.admin.db.bo.order.CartGoodsBo;
import com.example.admin.db.pojo.Goods;
import com.example.admin.db.pojo.OrderItem;
import org.apache.ibatis.annotations.Mapper;

import java.math.BigDecimal;
import java.util.List;

@Mapper
public interface GoodsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Goods record);

    int insertSelective(Goods record);

    Goods selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Goods record);

    int updateByPrimaryKey(Goods record);
    
    List<Goods> selectByKeyword(String keyword);
    
    List<Goods> selectByCategoryId(int categoryId);
    
    BigDecimal selectPriceByGoodsId(int goodsId);
    
    int batchUpdateQuantity(List<CartGoodsBo> cartGoodsBoList);
    
    int batchUpdateQuantityForCancelOrder(List<OrderItem> orderItemList);
}