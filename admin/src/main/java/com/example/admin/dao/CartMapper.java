package com.example.admin.dao;

import com.example.admin.db.bo.order.CartGoodsBo;
import com.example.admin.db.pojo.Cart;
import com.example.admin.db.pojo.Goods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CartMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Cart record);

    int insertSelective(Cart record);

    Cart selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Cart record);

    int updateByPrimaryKey(Cart record);
    
    /**
     * 返回用户所有购物车
     * @param userId
     * @return
     */
    List<Cart> selectByUserId(Integer userId);
    
    /**
     * 返回用户所有勾选或未勾选的购物车
     * @param userId
     * @param selected
     * @return
     */
    List<Cart> selectByUserIdSelected(@Param("userId") Integer userId, @Param("selected") int selected);

    /**
     * 返回指定用户所有购物车以及对应商品
     * @param userId
     * @return
     */
    List<com.example.admin.db.bo.CartGoodsBo> selectAllCartGoods(Integer userId);

    int batchUpdate(List<Cart> cartList);

    int batchUpdateTest(List<Cart> cartList);

    /**
     * 返回用户所有勾选的购物车对应的商品信息
     * @param userId
     * @return
     */
    List<Goods> selectGoodsByUserId(Integer userId);

    List<CartGoodsBo> querySelectedCartGoods(Integer userId);

    int batchDelete(List<CartGoodsBo> cartGoodsBoList);

    /**
     * 返回用户指定购物车
     */
    Cart selectCartByGoodsId(@Param("userId") Integer userId, @Param("goodsId") Integer goodsId);

    int deleteByCartIdUserId(@Param("userId") Integer userId, @Param("cartId") Integer cartId);
}