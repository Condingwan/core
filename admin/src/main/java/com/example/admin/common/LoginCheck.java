package com.example.admin.common;

import com.example.admin.dao.RoleMapper;
import com.example.admin.db.pojo.Role;
import com.example.admin.db.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;

@Component("loginCheck")
public class LoginCheck {

	@Autowired
	private RoleMapper roleMapper;
	
	/**
	 * 没登录:	false<br>
	 * 权限不对:	false
	 * @param session
	 * @param roleNo
	 * @return
	 */
	public boolean check(HttpSession session, int roleNo){
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		if(user == null)
			return false;	// 未登录
		Role role = roleMapper.selectByUserId(user.getId());
		//if(role.getRoleNo() != roleNo)
			//return false;	// 权限不对
		return true;
	}

}
