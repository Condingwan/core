
function login(){

	var formData = {
		"username":document.getElementById("username").value,
		"password":document.getElementById("password").value
	};
	$.ajax({
        url: "/go/user/login",
        type: "post",
        data: JSON.stringify(formData),
        contentType: "application/json",
        dataType: "json",
        success: function(result) {
            console.log(result);
            if(result.status == 0){
        		//alert("登录成功");
				window.location.href="/go/editInfo";
        	}else{
        		alert(result.msg);
        	}
		},
		error: function(){
            history.go(-1);
		}
	});
}