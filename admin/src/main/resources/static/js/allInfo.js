$(function () {
    //0.设置当前登录的用户
    // 该数据会被初始化，更新，插入使用到
    //2.初始化
    var oTable = new TableInit();
    oTable.Init();
});

var TableInit = function() {
    var oTableInit = new Object();

    //判断是否是管理员,为true，表示是管理员，为false，表示不是管理员
    if(false){
        //得到查询的参数
        oTableInit.queryParams = function(params) {
            var json={
                pageCond: {
                    shdCount: true,
                    pageSize: params.limit,
                    pageIndex: (params.offset / params.limit) + 1
                },
                query: {
                    sysCodeId: $('#sysCodeId').val(),
                    sysCodeName: $('#sysCodeName').val(),
                    updateUser:$('#updateUser').val()
                }
            }
            return json;

        };
    }else {
        $("#isAdmin").css("display","none");
        //得到查询的参数
        oTableInit.queryParams = function(params) {
            var json={
                pageCond: {
                    shdCount: true,
                    pageSize: params.limit,
                    pageIndex: (params.offset / params.limit) + 1
                },
                query: {
                    sysCodeId: $('#sysCodeId').val(),
                    sysCodeName: $('#sysCodeName').val(),
                    updateUser: storage.getItem("currentUser")
                }
            }
            return json;
        };
    }

    oTableInit.Init = function() {
        $('#tb_departments').bootstrapTable({
            url : '/go/user/getAllUserInfo', //请求后台的URL（*）
            method : 'post', //请求方式（*）
            toolbar : '#toolbar', //工具按钮用哪个容器
            striped : true, //是否显示行间隔色
            cache : false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
            pagination : true, //是否显示分页（*）
            contentType : "application/json",//请求的数据格式
            dataType:"json",//返回的数据格式
            sortable : false, //是否启用排序
            sortOrder : "asc", //排序方式
            queryParams : oTableInit.queryParams,//传递参数（*）
            sidePagination : "server", //分页方式：client客户端分页，server服务端分页（*）
            pageNumber : 1, //初始化加载第一页，默认第一页
            pageSize : 10, //每页的记录行数（*）
            pageList : [ 10, 25, 50, 100 ], //可供选择的每页的行数（*）
            search : false, //是否显示表格搜索
            strictSearch : true,
            showColumns : false, //是否显示所有的列
            showRefresh : false, //是否显示刷新按钮
            showLoading:false,//关闭显示数据未显示正在加载的信息提示
            minimumCountColumns : 2, //最少允许的列数
            /* clickToSelect : true, //是否启用点击选中行  */
            // uniqueId : "sessionid", //每一行的唯一标识，一般为主键列
            showToggle : false, //是否显示详细视图和列表视图的切换按钮
            cardView : false, //是否显示详细视图
            detailView : false, //是否显示父子表
            responseHandler:function(res){
                console.log(res);
                return {
                    "total":res.content.result.pageCond.count,
                    "rows": res.content.result.list
                };
            },
            columns : [ {
                field : 'id',
                title : '序号',
                formatter : serialNumberFormatter
            },{
                field : 'sysCodeId',
                title : '系统码',
            },{
                field : 'sysCodeName',
                title : '应用名称',
            },{
                field : 'sysCodeEncode',
                title : '应用编码',
            },{
                field : 'sysCodeStatus',
                title : '状态',
                formatter: statusFormatter,
            },{
                field : 'updateUser',
                title : '更新人',
            },{
                field : 'updateTime',
                title : '更新时间',
            },{
                field : 'execute',
                title : '操作',
                formatter : executeFormatter
            }],
            onLoadSuccess: function (res) {
            },
            onLoadError: function () {
                layer.msg('数据加载失败', {
                    icon : 5,
                    time : 2000
                });
            }
        });
        $("#tb_departments").bootstrapTable('hideLoading');
    };

    return oTableInit;
};