(function($){
$.extend({
pagination:function(obj,pageCond){
	$(obj).empty();
	var pageIndex = pageCond.pageIndex;//当前页码
	var totalPage = Math.ceil(pageCond.count / pageCond.pageSize);//总页数
    var showPageNum = 7;//页面显示页码的数量
    var page = '<a href="javascript:;" class="prevPage" onclick="queryData('+ (pageIndex-1) +')"><span class="fa fa-caret-left" style="color:#1DA2F0;font-size:14px;margin-right:7px"></span>上一页</a>'
    		   + '<a href="javascript:;" class="firstPage" onclick="queryData(1)">1</a>'
		       + '<a href="javascript:;" class="lastPage" onclick="queryData('+ totalPage +')">'+ totalPage +'</a>'
		       + '<a href="javascript:;" class="nextPage" onclick="queryData('+ (pageIndex+1) +')">下一页<span class="fa fa-caret-right" style="color:#1DA2F0; font-size:14px;margin-left:7px"></span></a>'
    $(obj).append(page);
    if(totalPage <= showPageNum){
    	var i;
    	for( i=totalPage-1; i>1; i--){
    		$(obj).children(".firstPage").after('<a href="javascript:;" class="midPage" onclick="queryData('+ i +')">'+i+'</a>')
    	}
    }
    else if(totalPage > showPageNum){
    	if(pageIndex < 5){
    		var i;
    		for( i=showPageNum-1; i>1; i--){
				if(i === pageIndex){
					$(obj).children(".firstPage").after('<a href="javascript:;" class="midPage current">'+i+'</a>')
        		}
				else{
					$(obj).children(".firstPage").after('<a href="javascript:;" class="midPage" onclick="queryData('+ i +')">'+i+'</a>')
				}
        		
        	}
    		$(obj).children(".lastPage").before('<span style="color: #999999;">&nbsp;...&nbsp;</span>');
    	}
    	else if(pageIndex > totalPage-4){
    		$(obj).children(".lastPage").before('<span style="color: #999999;">&nbsp;...&nbsp;</span>');
    		var i;
    		for( i=totalPage-showPageNum+2; i<totalPage; i++){
    			if(i === pageIndex){
    				$(obj).children(".lastPage").before('<a href="javascript:;" class="midPage current">'+i+'</a>')
    			}
    			else{
            		$(obj).children(".lastPage").before('<a href="javascript:;" class="midPage" onclick="queryData('+ i +')">'+i+'</a>')
    			}
        	}
    	}
    	else{
        	$(obj).children(".firstPage").after('<span style="color: #999999;">&nbsp;...&nbsp;</span>');
        	$(obj).children(".firstPage").after('<a href="javascript:;" class="midPage" onclick="queryData('+ (pageIndex+2) +')">'+ (pageIndex+2) +'</a>')
        	$(obj).children(".firstPage").after('<a href="javascript:;" class="midPage" onclick="queryData('+ (pageIndex+1) +')">'+ (pageIndex+1) +'</a>')
        	$(obj).children(".firstPage").after('<a href="javascript:;" class="midPage current">'+ (pageIndex) +'</a>')
        	$(obj).children(".firstPage").after('<a href="javascript:;" class="midPage" onclick="queryData('+ (pageIndex-1) +')">'+ (pageIndex-1) +'</a>')
        	$(obj).children(".firstPage").after('<a href="javascript:;" class="midPage" onclick="queryData('+ (pageIndex-2) +')">'+ (pageIndex-2) +'</a>')
        	$(obj).children(".firstPage").after('<span style="color: #999999;">&nbsp;...&nbsp;</span>');
        }
    }
    if(pageIndex === 1){
		$(obj).children(".firstPage").addClass("current");
		$(obj).children(".prevPage").remove()
		$(obj).children(".firstPage").before('<span class="disabled" style="font-size: 12px;"><span class="fa fa-caret-left" style="color:#DBDBDB;font-size:14px;margin-right:7px"></span>上一页</span>')
	}
	else if(pageIndex === totalPage){
		$(obj).children(".lastPage").addClass("current");
		$(obj).children(".nextPage").before('<span class="disabled" style="font-size: 12px;">下一页<span class="fa fa-caret-right" style="color:#DBDBDB;font-size:14px;margin-left:7px"></span></span>')
		$(obj).children(".nextPage").remove()
	}
}
})
})(jQuery)