document.write("<link rel=\"stylesheet\" href=\"../common/css/pagination.css\" />");
document.write("<script type=\"text/javascript\" src=\"../common/js/jquery.twbsPagination.js\"></script>");

//url,defaultPageSize,queryBtnSelector,enterEventSelector,tbodySelector,paramsCallback,htmlCallback
function initPagination(pageInitObj){
	var pageFootDiv = 
		"<span class=\"pageFoot\">"+
			"<span class=\"p_left\" style=\"float: left; margin-left: 20px; margin-top: 15px; font-size: 15px; color: #2283c5;\">"+
					"<font class=\"pilabel\"> </font>"+
			"</span> "+
			"<span class=\"p_right\" style=\"float: right; margin-top: 0px; margin-right: 5px;\"></span>"+
		"</span>";
		$("body").append(pageFootDiv);
		
		//每页大小
		var limit = pageInitObj.defaultPageSize?pageInitObj.defaultPageSize:8;
		loadData();
		//查询按钮
		if(pageInitObj.queryBtnSelector){
			$(pageInitObj.queryBtnSelector).on("click",function(){
				loadData();
			});
		}
		//enter触发
		if(pageInitObj.enterEventSelector){
			$(pageInitObj.enterEventSelector).on("keydown",function(event){
				alert(event.keyCode);
				if(event.keyCode==13){
					loadData();
				}
			});
		}
		function loadData(paramObj){
			//layer.load();
			var data;
			if(pageInitObj.paramsCallback){
			 var data =	pageInitObj.paramsCallback()||{};
			}else{
				data = {};
			}
			if(paramObj){
				for(var key in paramObj){
					data[key] = paramObj[key];
				}
			}
			data.limit = limit;
			$.ajax({
				url:pageInitObj.url,
				data:data,
				dataType:'json',
				type:"post",
				success:function(result){
					if(result.code == 0||result.code == '0'){
						renderingData(result);
					}else{
						layer.msg(result.msg);
						renderingData();
					}
					//layer.closeAll('loading');
				},
				error:function(a,b,c){
					console.dir(a);
					console.dir(b);
					console.dir(c);
					renderingData();
					//layer.closeAll('loading');
					//layer.msg("系统繁忙"); 
					layer.msg('检测不到sso登录信息，请重新登录', {
						icon : 5,
						time : 2000
					});
				}
			});
		}
		function renderingData(json){
			var totalPages = 0;
			var startPage = 0;
			var total = 0;
			if(json){
				$(pageInitObj.tbodySelector).html(pageInitObj.htmlCallback(json));
				totalPages = (json.content.total%limit==0)?json.content.total/limit:((json.content.total-json.content.total%limit)/limit+1);
				startPage = json.content.total==0?0:(json.content.start/limit)+1;
				var total = json.content.total;
			}else{
				$(pageInitObj.tbodySelector).html("<tr ><td colspan=\"100\" style=\"text-align: center;\" ><font color=\"red\">数据加载失败</font></td></td>");
			}
			var pilabelHtml = "当前 " + startPage + "/" + totalPages + "页,共"	+ total + "条记录";
			if(startPage!=0){
				pilabelHtml+= ",转<font style=\"position: relative;top: 0px;\"><input class=\"goInput\" data-max=\""+totalPages+"\" style=\"width:20px;height:16px;\" type=\"text\" /></font>";
			}
			$(".pilabel").html(pilabelHtml);
			$(".p_right").remove();
			$(".pageFoot").append("<span class=\"p_right\" style=\"float: right; margin-top: 0px; margin-right: 5px;\"></span>");
			
			if(total>0){
				$(".pageFoot .p_right").twbsPagination({
					totalPages : totalPages,
					startPage : startPage,
					visiblePages : 5,
					onPageClick : function(event, curPage) {
						loadData({"start":(curPage-1)*limit}); 
					}
				});
			}
			
		}
		//转到多少页
		$(".pageFoot").on("keydown",".goInput",function(event){
			if(event.keyCode==13){
				try {
					var maxPageNo = parseInt($(this).attr("data-max"));
					var pn = parseInt($(this).val());
					if(pn<=maxPageNo && pn>0){
						loadData({"start":(pn-1)*limit});
					}else if(pn>maxPageNo){
						layer.msg("页码超过限制");
						$(this).val("");
					}else{
						layer.msg("请输入正确页码");
						$(this).val("");
					}
				} catch (e) {
					layer.msg("请输入正确页码");
					$(this).val("");
				}
			}
			
		});
		//刷新数据表格
		freshDataBody = function(obj){
			loadData(obj||{});
		}
		
		//刷新当前页
		freshCurPage = function(){
			freshDataBody({"start":(parseInt($(".pagination .active a").text())-1)*limit});
		}
}
var freshDataBody;
var freshCurPage;



