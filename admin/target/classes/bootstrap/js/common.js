/*document.write("<script type=\"text/javascript\" src=\"../common/jquery/jquery-3.1.1.min.js\"></script>");
document.write("<script type=\"text/javascript\" src=\"../common/bootstrap/js/bootstrap.min.js\"></script>");*/


//获得参数的方法
var request = {
	QueryString : function(val) { 
		var uri = window.location.search; 
		var re = new RegExp("" +val+ "=([^&?]*)", "ig"); 
		return ((uri.match(re))?(uri.match(re)[0].substr(val.length+1)):null); 
	} 
}

var role = request.QueryString("role");

function checkRole(role) {
	if(role == 'approval') {
		$("a[role=approval]").removeAttr("style");
		$("a[role=admin]").attr("style","display:none;");
		$("a[role=user]").attr("style","display:none;");
	} else if(role == 'admin') {
		$("a[role=admin]").removeAttr("style");
		$("a[role=approval]").attr("style","display:none;");
		$("a[role=user]").attr("style","display:none;");
	} else if(role == 'user') {
		$("a[role=user]").removeAttr("style");
		$("a[role=approval]").attr("style","display:none;");
		$("a[role=admin]").attr("style","display:none;");
	}
}
//check box点击事件
function checkboxChange(e) {
	if($(e).attr('checked') =='checked') {
		$(e).removeAttr('checked');
	} else {
		$(e).attr('checked','checked');
	}
}

//radio点击事件
function radioChange(e) {
	if($(e).attr('checked') !='checked') {
		$(e).parent().parent().parent().find("input[name=fieldPrimary]").each(function() {
			$(this).removeAttr('checked');
		});
		$(e).attr('checked','checked');
	}
}

function backConfirm(type, url){
	layer.confirm('确认返回？', {
		icon: 3,
		title: '重要提示',
		btn : [ '确定', '取消' ]
	//按钮
	}, function() {
		location.href= url;
	}, function() {

	});
}

function backDirect(url){
	location.href=url;
}