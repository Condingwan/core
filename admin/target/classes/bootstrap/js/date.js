	
function setDate(a)
{
	$('input[name="'+a+'"]').daterangepicker({
        "autoApply": false, //选择日期后自动提交;只有在不显示时间的时候起作用timePicker:false
        singleDatePicker: true, //单日历
        showDropdowns: false, //年月份下拉框
        timePicker: false, //显示时间
        timePicker24Hour: false, //时间制
        timePickerSeconds: false, //时间显示到秒
        startDate: moment().hours(0).minutes(0).seconds(0), //设置开始日期
        maxDate: moment(new Date()), //设置最大日期
        applyClass: "btn btn-primary",
        "opens": "center",
        showWeekNumbers: false,
        locale: {
            format: "YYYY-MM-DD", //设置显示格式
            //applyLabel: '确定', //确定按钮文本
            //cancelLabel: '取消', //取消按钮文本
            daysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
            monthNames: ['一月', '二月', '三月', '四月', '五月', '六月',
                '七月', '八月', '九月', '十月', '十一月', '十二月'],
            firstDay: 1
        },
        format: "YYYY-MM-DD"
    }, function(start) {
        console.log(start.format('YYYY-MM-DD'));
    })
}
function setDoubleDate(a,b){
	var minDate = null;
    var max = null;
    function fromDate(maxDate) {
        if(!maxDate){
            max = moment(new Date())
        }else{
            max = maxDate;
        }
        $('input[name="'+a+'"]').daterangepicker({
            "autoApply": true, //选择日期后自动提交;只有在不显示时间的时候起作用timePicker:false
            singleDatePicker: true, //单日历
            showDropdowns: false, //年月份下拉框
            timePicker: false, //显示时间
            timePicker24Hour: true, //时间制
            timePickerSeconds: true, //时间显示到秒
            // startDate: moment().hours(0).minutes(0).seconds(0), //设置开始日期
            maxDate: max , //设置最大日期
            "opens": "center",
            applyClass: "btn btn-primary",
            showWeekNumbers: false,
            locale: {
                format: "YYYY-MM-DD", //设置显示格式
                applyLabel: '确定', //确定按钮文本
                cancelLabel: '取消', //取消按钮文本
                daysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
                monthNames: ['一月', '二月', '三月', '四月', '五月', '六月',
                    '七月', '八月', '九月', '十月', '十一月', '十二月'
                ],
                firstDay: 1
            },
            format: "YYYY-MM-DD"
        }, function(s) {
            toDate(s);
        });
    }
    fromDate()
    function toDate(minDate) {
        $('input[name="'+b+'"]').daterangepicker({
            "autoApply": true, //选择日期后自动提交;只有在不显示时间的时候起作用timePicker:false
            singleDatePicker: true, //单日历
            showDropdowns: false, //年月份下拉框
            timePicker: false, //显示时间
            timePicker24Hour: true, //时间制
            timePickerSeconds: true, //时间显示到秒
            // startDate: moment().hours(0).minutes(0).seconds(0), //设置开始日期
            maxDate: moment(new Date()), //设置最大日期
            minDate: minDate,
            "opens": "center",
            applyClass: "btn btn-primary",
            showWeekNumbers: false,
            locale: {
                format: "YYYY-MM-DD", //设置显示格式
                applyLabel: '确定', //确定按钮文本
                cancelLabel: '取消', //取消按钮文本
                daysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
                monthNames: ['一月', '二月', '三月', '四月', '五月', '六月',
                    '七月', '八月', '九月', '十月', '十一月', '十二月'
                ],
                firstDay: 1
            },
            format: "YYYY-MM-DD"
        }, function(s) {
            fromDate(s)
        });
    }
    toDate();
}