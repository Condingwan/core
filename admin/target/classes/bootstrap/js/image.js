function handleImg(obj){
	//建立空数组，用于存放获取的文件路径及相关的判断操作
	var arr = [];
	//当input的值发生变化就触发事件（利用值的变化判断是否选取了文件）

	$(obj).change(function() {
		$("#list-view").show();
	    //限制选取文件的最大数量为4
	    if (arr.length > 3) {
	        //将input设置为禁用状态
	        $('#fileUpload').attr('disabled','disabled');
	    }       
	    //获取选取的文件路径并解析为可渲染的路径
	    var imgSrc = URL.createObjectURL(this.files[0]);
	    //将路径添加至img标签中
	    var template = '<div style="border: 1px solid #DFDFDF;width:160px;height:160px;margin:20px;float:left;">'+
	    			   '<img src="' + imgSrc + '" style="width:158px;margin-top:20%">'+
	    			   '<a id="deleteImg" href="#" class="btn" style="background: #1DA2F0;border-radius:0;border:none;margin-left:125px; margin-top:13px; width:34px; height:34px;padding:8px 0 3px 0;">'+
	    		       '<span class="glyphicon glyphicon-trash" style="color:#ffffff"></span>'+
	    		       '</a></div>';
	    //将img标签插入元素中，在页面显示
	    $('#list-view').append(template);  
	    //将路径插入数组中，用于已选取文件数量判断
	    arr.push(imgSrc);   
	});

	//为防止ajax动态添加数据无法绑定事件，故用body添加on事件给子元素添加，避开这个问题
	$('body').on('click','#deleteImg',function(){
	    //获取当前点击的元素下标（其下标与数组下标是一致的）
	    var i = $(this).index();
	    //判断如果当前选取文件数量未超过4张而设置input为非禁用状态
	    if (arr.length < 5) {
	        //清除input禁用状态
	        $('#fileUpload').removeAttr("disabled");
	    }           
	    //根据点击删除元素的下标，删除数组下标（img标签只是负责渲染，但数据源是arr数组，判断都是依靠他，所以必须删除数组里面的数据）
	    arr.splice(i,i);
	    //当前点击元素下标为0（即第一个时，清空数组）
	    if (i == 0) {
	        arr.splice(0,1);
	    }
	    //删除当前点击元素
	    $(this).parent().remove();
	    $(this).remove();
	});
}