
$(document).ready(function(){
	
	$('input[type=checkbox],input[type=radio],input[type=file]').uniform();
	
	$('select').select2();
    $('.colorpicker').colorpicker();
    $('.datepicker').datepicker();
});

$(document).ready(function() { 	
	
	//------------- Datepicker -------------//
	if($('#datepicker').length) {
		$("#datepicker").datepicker({
			showOtherMonths:true
		});
	}
	if($('#datepicker-inline').length) {
		$('#datepicker-inline').datepicker({
	        inline: true,
			showOtherMonths:true
	    });
	}

	//------------- Combined picker -------------//
	if($('#combined-picker').length) {
		$('#combined-picker').datetimepicker();
	}
	
   /* //------------- Time entry (picker) -------------//
	$('#timepicker').timeEntry({
		show24Hours: true,
		spinnerImage: ''
	});
	$('#timepicker').timeEntry('setTime', '22:15')*/

	//------------- Select plugin -------------//
	$("#select1").select2();
	$("#select2").select2();

	
});//End document ready functions


