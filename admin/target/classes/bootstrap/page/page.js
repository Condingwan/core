/*创建分页
         **@ pageIndex  当前页
         **@ pageSize   每页记录数量
         **@ totalCount 总记录条数
         */
		function createPagination(pageIndex, pageSize, totalCount) {
			var totalPage = Math.ceil(totalCount / pageSize);//总页数
			var showPageNum = 10;//页面显示页码的数量
			var pageNumString = '';
			var realPageIndex = pageIndex;
			//只显示showPageNum 个页码-------------------------------
			if (totalPage <= showPageNum) {
				pageIndex = 1;
			} else if ((totalPage - pageIndex) < (showPageNum - 1)) {
				pageIndex = (totalPage - showPageNum + 1);
			}
			//只显示showPageNum 个页码-------------------------------
			for (var i = 0; i < totalPage; i++) {
				if ((pageIndex + i) > totalPage) {
					break;
				}
				if (i >= showPageNum) {
					break;
				}

				if ((pageIndex + i) == realPageIndex) {
					pageNumString += '<li class="active"><a href="javascript:void(0);"onclick="queryData('
							+ (pageIndex + i)
							+ ')">'
							+ (pageIndex + i)
							+ '</a></li>';
				} else {
					pageNumString += '<li><a href="javascript:void(0);"onclick="queryData('
							+ (pageIndex + i)
							+ ')">'
							+ (pageIndex + i)
							+ '</a></li>';
				}
			}
			var pageIndexPre = (realPageIndex - 1) <= 0 ? 1 : (realPageIndex - 1);//前一页
			var pageIndexNext = (realPageIndex + 1) >= totalPage ? totalPage
					: (realPageIndex + 1);//后一页
			var page = "<nav>"
					+ '<ul class="pagination"> '
					+ '<li><a><span aria-hidden="true">共 '
					+ totalCount
					+ ' 条</span></a></li>'
					+ '<li><a href="javascript:void(0);" onclick="queryData(0);" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>'
					+ '<li><a href="javascript:void(0);" onclick="queryData('
					+ pageIndexPre
					+ ');" aria-label="Previous">'
					+ '<span aria-hidden="true">&lsaquo;</span></a></li>'
					+ pageNumString
					+ '<li><a href="javascript:void(0);" onclick="queryData('
					+ pageIndexNext
					+ ');" aria-label="Next"> '
					+ '<span aria-hidden="true">&rsaquo;</span></a></li>'
					+ '<li><a href="javascript:void(0);" onclick="queryData('
					+ totalPage
					+ ');" aria-label="Previous"><span aria-hidden="true">&raquo;</span></a></li>'
					+ '</ul>' + '</nav>';
			$('#page').empty();
			$('#page').append(page);
		}