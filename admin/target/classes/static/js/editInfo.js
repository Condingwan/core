var userInfoJson = null;
var userAllInfoJson = null;

$(document).ready(function(){
	$.ajax({
        url: "/go/user/getInfo",
        type: "post",
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(result) {
        	userInfoJson = result;
        	showUserName();
		},
	});
	$.ajax({
        url: "/go/user/getAllUserInfo",
        type: "post",
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(result) {
        	if(result.status == 0){
	        	data1 = result.data;
				var table=document.getElementById("table");
				for(var i=0;i<data1.length;i++){
					var row=table.insertRow(table.rows.length);
					var c1=row.insertCell(0);
					c1.innerHTML=data1[i].username;
					var c2=row.insertCell(1);
					c2.innerHTML=data1[i].phone;
					var c3=row.insertCell(2);
					c3.innerHTML=data1[i].email;
					var c4=row.insertCell(3);
					c4.innerHTML=data1[i].question;
					var c5=row.insertCell(4);
					c5.innerHTML=data1[i].answer;
					var c6=row.insertCell(5);
					c6.innerHTML=data1[i].id;
				}
        	}
		},
	});
	$.ajax({
		url: "/go/user/getAllInfo",
		type: "post",
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(result) {
			if(result.status == 0){
				userAllInfoJson = result;
				showInfo();
			}
		},
	});

	$("#modify").click(function(){
		var formData = new FormData();
		formData.append("id", $("#id").val());
		formData.append("phone", $("#phone").val());
		formData.append("email", $("#email").val());
		$.ajax({
	        url: "user/editInfo",
	        type: "post",
	        data: formData,
	        contentType: false,
	        processData: false,
	        dataType: "json",
	        success: function(result) {
	        	if(result.status == 0){
		        	alert(result.msg);
		        	location.reload();
	        	}else{
	        		alert(result.msg);
	        	}
			},
			error: function(){
				alert("error进入");
			}
		});
	});
});

function showInfo(){
	if($.isEmptyObject(userAllInfoJson))
		return;
	if(userInfoJson.status == 1){
		return;
	}
	
	var name = userAllInfoJson.data.username;
	console.log(name);
	var phone = userAllInfoJson.data.phone;
	var email = userAllInfoJson.data.email;
	var question = userAllInfoJson.data.question;
	var answer = userAllInfoJson.data.answer;
	$("#username").attr("placeholder", name);
	$("#phone").attr("placeholder", phone);
	$("#email").attr("placeholder", email);
	$("#question").attr("placeholder", question);
	$("#answer").attr("placeholder", answer);
	
}

function showUserName(){
	if($.isEmptyObject(userInfoJson))
		return;
	if(userInfoJson.status == 1){
		location.href = "login.html";
	}
	
	// 删除login、register节点
	document.getElementById("nav-ul").removeChild(document.getElementById("login"));
	document.getElementById("nav-ul").removeChild(document.getElementById("register"));
	
	document.getElementById("nav-username").style.display = "";
	document.getElementById("logout").style.display = "";
	document.getElementById("nav-username").innerHTML = "你好，" + userInfoJson.data.username;
	
	document.getElementById("username").setAttribute("placeholder", userInfoJson.data.username);
}