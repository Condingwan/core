var userInfoJson = null;
var userAllInfoJson = null;

$(document).ready(function(){
	$(function () {
		var oTable = new TableInit();
		oTable.Init();
	});
	$.ajax({
        url: "/go/user/getInfo",
        type: "post",
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(result) {
        	userInfoJson = result;
        	showUserName();
		},
	});

	$("#modify").click(function(){
		var formData = new FormData();
		formData.append("id", $("#id").val());
		formData.append("phone", $("#phone").val());
		formData.append("email", $("#email").val());
		$.ajax({
	        url: "user/editInfo",
	        type: "post",
	        data: formData,
	        contentType: false,
	        processData: false,
	        dataType: "json",
	        success: function(result) {
	        	if(result.status == 0){
		        	alert(result.msg);
		        	location.reload();
	        	}else{
	        		alert(result.msg);
	        	}
			},
			error: function(){
				alert("error进入");
			}
		});
	});
});
var TableInit = function() {
	var oTableInit = new Object();
	oTableInit.Init = function(){
		$('#table_template').bootstrapTable({
		method: "post",  		//提交方法
		striped: true,			//是否显示行间隔色
		cache:false,			//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性
		singleSelect: false,		//设置True 将禁止多选
		url: "/go/order/listAllOrder",//查询的地址
		dataType: "json",			//服务器返回的数据类型
		pagination: false, 			//是否分页
		pageSize: 5,				//页大小
		pageNumber: 1,			//当前页
		search: false, //显示搜索框
		sidePagination : "server",
		contentType: "application/x-www-form-urlencoded",	//发送到服务器的数据编码类型
		queryParams:null,			//参数
		/* clickToSelect : true, //是否启用点击选中行  */
		// uniqueId : "sessionid", //每一行的唯一标识，一般为主键列
		showToggle : false, //是否显示详细视图和列表视图的切换按钮
		cardView : false, //是否显示详细视图
		detailView : false, //是否显示父子表
		responseHandler:function(res){
			console.log(res.data.list);
			return {
				"total":res.data.total,
				"rows": res.data.list
			};
		},
		columns: [
			{
				title: 'id',		//列标题
				field: 'id',				//该列映射的data的参数名
				align: 'center',			//水平对齐方式
				valign: 'middle',		//垂直对齐方式
			}, {
				title: '金额',
				field: 'payment',
				align: 'center',
				valign: 'middle',
			}, {
				title: '状态',
				field: 'status',
				align: 'center',
				valign: 'middle',
				formatter: statusFormatter,
			}, {
				field: '',
				title: '操作',
				halign: 'center',
				align: 'center',
				width: '150px',			//宽度
				//sortable: true,			//下面是单独加的按钮
				formatter : executeFormatter
			}],
			onLoadSuccess: function (res) {
				console.log(res);
			},
			onLoadError: function () {
				layer.msg('数据加载失败', {
					icon : 5,
					time : 2000
				});
			}
	});
		$("#table_template").bootstrapTable('hideLoading');
	};
	return oTableInit;
}
function statusFormatter(value, row, index){
	if(value!="2"){
		return '<span class=\'label label-success\' >未发货</span>&nbsp;&nbsp;'
	}else {
		return '<span class=\'label label-default\' style="background-color: #D0D0D0;">已发货</span>&nbsp;&nbsp;';
	}

}
function executeFormatter(value, row, index){
	var editHtml = '<a href="#" onclick="updateStatus(this)" id="'+row.id+'" status="'+row.status+'" >&nbsp;&nbsp;发货&nbsp;&nbsp;</a>';
	// var betweenHtml = '<span style="color:#1DA2F0">|</span>';
	// var deleteHtml = '<a href="#" onclick="errorListView('+row.sysCodeId+')">&nbsp;&nbsp;错误码列表&nbsp;&nbsp;</a>';
	return editHtml /*+ betweenHtml + deleteHtml*/ ;
}
function updateStatus(obj){
	alert("111");
	var status = 2;
	$.ajax({
		type: "POST",
		url: "/go/order/updateStatus",
		contentType:"application/json",
		data :JSON.stringify({
			id:$(obj).attr("id"),
			status:status
		}),
		dataType: "json",
		success: function(data) {
			refreshTable();
		},
		error: function(){
			refreshTable();
		}

	});
}
function refreshTable(){
	$("#table_template").bootstrapTable('destroy');
	//1.初始化
	var oTable = new TableInit();
	oTable.Init();
	$("#table_template").css("margin-top","20px");
}
function showUserName(){
	if($.isEmptyObject(userInfoJson))
		return;
	if(userInfoJson.status == 1){
		location.href = "login.html";
	}
	
	// 删除login、register节点
	document.getElementById("nav-ul").removeChild(document.getElementById("login"));
	document.getElementById("nav-ul").removeChild(document.getElementById("register"));
	
	document.getElementById("nav-username").style.display = "";
	document.getElementById("logout").style.display = "";
	document.getElementById("nav-username").innerHTML = "你好，" + userInfoJson.data.username;
	
	document.getElementById("username").setAttribute("placeholder", userInfoJson.data.username);
}