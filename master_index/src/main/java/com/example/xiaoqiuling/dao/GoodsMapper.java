package com.example.xiaoqiuling.dao;

import java.math.BigDecimal;
import java.util.List;

import com.example.xiaoqiuling.db.bo.order.CartGoodsBo;
import com.example.xiaoqiuling.db.pojo.Goods;
import com.example.xiaoqiuling.db.pojo.OrderItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GoodsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Goods record);

    int insertSelective(Goods record);

    Goods selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Goods record);

    int updateByPrimaryKey(Goods record);
    
    List<Goods> selectByKeyword(String keyword);
    
    List<Goods> selectByCategoryId(int categoryId);
    
    BigDecimal selectPriceByGoodsId(int goodsId);
    
    int batchUpdateQuantity(List<CartGoodsBo> cartGoodsBoList);
    
    int batchUpdateQuantityForCancelOrder(List<OrderItem> orderItemList);
}