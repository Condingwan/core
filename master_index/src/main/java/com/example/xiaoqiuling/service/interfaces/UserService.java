package com.example.xiaoqiuling.service.interfaces;

import com.example.xiaoqiuling.db.pojo.User;
import com.example.xiaoqiuling.utils.Result;

public interface UserService {
    public Result loginInvalid(User user);

    public Result insertUser(User user);

}
