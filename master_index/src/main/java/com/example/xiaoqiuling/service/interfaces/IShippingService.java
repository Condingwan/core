package com.example.xiaoqiuling.service.interfaces;

import com.example.xiaoqiuling.common.Message;
import com.example.xiaoqiuling.db.pojo.Shipping;

public interface IShippingService {

	Message addShipping(Shipping shipping, Integer userId);
	
	Message list(Integer userId);
}
