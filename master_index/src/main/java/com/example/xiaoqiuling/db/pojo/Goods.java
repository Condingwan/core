package com.example.xiaoqiuling.db.pojo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class Goods {
    private Integer id;

    private String name;

    private Integer categoryId;

    private String subtitle;

    private String mainImage;

    private String detail;

    private BigDecimal price;

    private Integer stock;

    private Date createTime;

    private Date updateTime;

    public Goods(Integer id, String name, Integer categoryId, String subtitle, String mainImage, String detail, BigDecimal price, Integer stock, Date createTime, Date updateTime) {
        this.id = id;
        this.name = name;
        this.categoryId = categoryId;
        this.subtitle = subtitle;
        this.mainImage = mainImage;
        this.detail = detail;
        this.price = price;
        this.stock = stock;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }
}