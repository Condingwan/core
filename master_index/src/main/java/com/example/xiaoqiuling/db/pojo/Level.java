package com.example.xiaoqiuling.db.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class Level {
    private Integer id;

    private Integer userId;

    private Integer level;

    private Integer exp;

    private Date createTime;

    private Date updateTime;

    public Level(Integer id, Integer userId, Integer level, Integer exp, Date createTime, Date updateTime) {
        this.id = id;
        this.userId = userId;
        this.level = level;
        this.exp = exp;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public Level() {

    }
}