package com.example.xiaoqiuling.db.vo;

import com.example.xiaoqiuling.db.pojo.Goods;
import lombok.Data;

@Data
public class GoodsDetailVo {

	// Goods pojo
	private Goods goods;
	// extension
	private String imgHost;

}
