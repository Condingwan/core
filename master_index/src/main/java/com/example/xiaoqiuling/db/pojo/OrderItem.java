package com.example.xiaoqiuling.db.pojo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class OrderItem {
    private Integer id;

    private Integer userId;

    private Integer orderId;

    private Integer goodsId;

    private Long orderNo;

    private Integer quantity;

    private String name;

    private String mainImage;

    private Integer status;

    private String detail;

    private BigDecimal price;

    private BigDecimal totalPrice;

    private Date refundTime;

    private Date createTime;

    private Date updateTime;

    public OrderItem(Integer id, Integer userId, Integer orderId, Integer goodsId, Long orderNo, Integer quantity, String name, String mainImage, Integer status, String detail, BigDecimal price, BigDecimal totalPrice, Date refundTime, Date createTime, Date updateTime) {
        this.id = id;
        this.userId = userId;
        this.orderId = orderId;
        this.goodsId = goodsId;
        this.orderNo = orderNo;
        this.quantity = quantity;
        this.name = name;
        this.mainImage = mainImage;
        this.status = status;
        this.detail = detail;
        this.price = price;
        this.totalPrice = totalPrice;
        this.refundTime = refundTime;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public OrderItem() {

    }
}